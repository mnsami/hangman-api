# Hangman

This is a project to play around with docker, symfony, and angular 1.5.

### 1. Frameworks and bundles used
- Backend API
    1. `Symfony: 2.8`
    2. `justinrainbow/json-schema`, for using `JSON` schema validation of requests.
- Frontend
    1. Angular 1.5.11
    2. Bootstrap
    
### 2. Installations

1. Setup project and containers:

    1.1 checkout the project

        git clone git@gitlab.com:mnsami/hangman-api.git
    
    1.2. Start containers

        make container-up
    
2. Setup Backend API

    2.1. `cd api/`
    
    2.2. Run make, this will copy `app/config/parameters.yml.dist` to `app/config/parameters.yml`

        make
        
    2.3. Install composer dependencies
    
        make composer

    2.4. Database setup, credentials are set in the `.env` file, set the db password, user and name in `app/config/parameters.yml`

        make db-schema-create

3. Frontend

You don't have to do anything for the frontend.
You can access the frontend via "http://localhost:8080" from your browser.

### 3. API Available routes

The Backend api can be accessed via "http://localhost".


|  Name|                 Method|   Scheme|   Host|   Path|
|--------------------|--------|--------|------|---------------------|
|  create_game|          POST|     ANY|      ANY|    /v1/games|
|  post_game_move|       POST|     ANY|      ANY|    /v1/games/{id}/next|
|  get_games|            GET|      ANY|      ANY|    /v1/games|
|  alive|                GET|      ANY|      ANY|    /v1/_healthCheck|
|  create_player|        POST|     ANY|      ANY|    /v1/players|
|  get_player_by_uuid|   GET|      ANY|      ANY|    /v1/players/{uuid}|
|  get_players|          GET|      ANY|      ANY|    /v1/players|

#### 3.1 Api Documentation

All api calls **requires** the presence of `X-Hangman-Player-uuid` header. Excepts:
- `POST /v1/playes`
- `GET /v1/_healthCheck`

#### 3.1.1 `POST /v1/players`
- **Description:** Creates a new player.
- **method**: `POST`
- **Response**: Player created object.
- **Data Params**:
  - **username:** Player username, **type:** string, **required:** true
  - **age:** Player age, **type:** integer, **required:** true

#### 3.1.2 `GET /v1/players/{uuid}`
- **Description:** Get player by uuid.
- **method**: `GET`
- **Response**: Player object.
- **Url Params**:
  - **uuid:** Player uuid, **type:** string, **required:** true

#### 3.1.3 `GET /v1/players`
- **Description:** Get players.
- **method**: `GET`
- **TODO:** Add filter parameters and pagination.
- **Response:** List of players.

#### 3.1.4 `POST /v1/games`
- **method**: `POST`
- **Description:** Starts new game for player.
- **Response:** Game created object.

#### 3.1.5 `POST /v1/games/{id}/next`
- **method**: `POST`
- **Description:** Sets player's next move.
- **Response:** Game updated object.
- **Data Params:**
  - **letter:** letter chosen, **type:** string, **required:** true

#### 3.1.5 `GET /v1/games`
- **method**: `GET`
- **Description:** Get list of games.
- **Response:** List of games objects.
- **TODO:** Add filter parameters and pagination.


#### 3.2 Security
This is a simple `API`. It doesn't have any complicated registration with passwords. But we acheived a simple `security` technique by saving the user `player` in the `localstorage` of the browser.

For every new `player` there is a `uuid` created. It is required by every `api` call to send this `uuid` via setting `X-Hangman-Player-uuid` header.

Only the:
- `alive` and `create_game` routes are available without requiring the `X-Hangman-Player-uuid` header.


#### 4 Frontend screenshots
1. Homepage (unregistered)  
![Image of homepage_unregistered](./frontend/screenshots/hangman_unregistered.jpg)

2. Registration  
![Image of registration](./frontend/screenshots/hangman_register.jpg)

3. Homepage (registered)  
![Image of homepage](./frontend/screenshots/hangman_homepage.jpg)

4. Choose Game  
![Image of choose game](./frontend/screenshots/hangman_choose.jpg)

5. Hangman game play  
![Image of game play](./frontend/screenshots/hangman_game_play.jpg)

6. Player won game  
![Image of player won](./frontend/screenshots/hangman_won.jpg)

