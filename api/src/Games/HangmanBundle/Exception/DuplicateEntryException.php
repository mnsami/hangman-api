<?php

namespace Games\HangmanBundle\Exception;

class DuplicateEntryException extends Base\BaseException
{
    protected $httpStatusCode = 500;
}
