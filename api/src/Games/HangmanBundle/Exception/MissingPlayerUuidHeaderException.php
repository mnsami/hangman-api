<?php

namespace Games\HangmanBundle\Exception;

class MissingPlayerUuidHeaderException extends BadRequestException
{
}
