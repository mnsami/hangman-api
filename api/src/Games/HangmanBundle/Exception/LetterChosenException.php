<?php

namespace Games\HangmanBundle\Exception;

class LetterChosenException extends BadRequestException
{
}
