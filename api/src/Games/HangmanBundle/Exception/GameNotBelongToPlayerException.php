<?php

namespace Games\HangmanBundle\Exception;

class GameNotBelongToPlayerException extends Base\BaseException
{
    protected $httpStatusCode = 404;
}
