<?php

namespace Games\HangmanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;

use Games\HangmanBundle\Exception\PlayerNotFoundException;
use Games\HangmanBundle\Exception\ForbiddenException;

class GamesController extends FOSRestController
{
    /**
     * @Post("/games")
     */
    public function createGameAction(Request $request)
    {
        $playerUuid = $request->headers->get('X-Hangman-Player-uuid');
        $player = $this->get('doctrine')->getRepository('HangmanBundle:Player')->findPlayerByUuid($playerUuid);

        if (!$player) {
            throw new PlayerNotFoundException();
        }

        $activeGames = $this->get('doctrine')
            ->getRepository('HangmanBundle:Game')
            ->doesPlayerHasActiveGame($player->getId());
        if (!empty($activeGames)) {
            throw new ForbiddenException("There is already an active Game in progress.");
        }

        $gameHandler = $this->get("hangman.handler.game");
        $game = $gameHandler->startNewGame($player);
        $this->get('logger')->info('Creating game for player: ' . $player->getUuid());

        return View::create($game, 200);
    }

    /**
     * @Post("/games/{id}/next", defaults={"action" = "game"})
     */
    public function postGameMoveAction(Request $request, $id, $action)
    {
        $playerUuid = $request->headers->get('X-Hangman-Player-uuid');

        $requestContent = $request->getContent();
        $this->get('hangman.service.json_form_validator')->assertValidJobSchema($requestContent, $action);
        $this->get('logger')->info('Processing Player next move: ' . json_encode($requestContent));

        $player = $this->get('doctrine')->getRepository('HangmanBundle:Player')->findPlayerByUuid($playerUuid);

        if (!$player) {
            throw new PlayerNotFoundException();
        }

        $gameHandler = $this->get("hangman.handler.game");
        $game = $gameHandler->playNextMove($id, $player->getId(), $requestContent);

        return View::create($game, 200);
    }

    /**
     * @Get("/games")
     */
    public function getGamesAction(Request $request)
    {
        $playerUuid = $request->headers->get('X-Hangman-Player-uuid');
        $player = $this->get('doctrine')->getRepository('HangmanBundle:Player')->findPlayerByUuid($playerUuid);

        if (!$player) {
            throw new PlayerNotFoundException();
        }

        $gameHandler = $this->get("hangman.handler.game");
        $topGames = $gameHandler->getPlayerTopGames($player->getId());

        return View::create($topGames, 200);
    }
}
