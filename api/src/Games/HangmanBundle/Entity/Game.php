<?php

namespace Games\HangmanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Games\HangmanBundle\Entity\Player;

/**
 *
 * @ORM\Entity
 * @ORM\Table("games")
 *
 * @ORM\Entity(repositoryClass="Games\HangmanBundle\Entity\GameRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class Game
{
    const STATUS_WIN = "WIN";
    const STATUS_LOOSE = "LOOSE";
    const STATUS_INPROGRESS = "INPROGRESS";

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="games")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    private $player;

    /**
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;

    /**
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @ORM\Column(name="letters", type="json_array", nullable=true)
     */
    private $letters;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     *
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="word", type="string", length=255)
     */
    private $word;

    /**
     * @ORM\Column(name="failed_attempts", type="integer")
     */
    private $failedAttempts;

    /**
     * @ORM\Column(name="wrong_trials", type="json_array", nullable=true)
     */
    private $wrongTrials;

    public function __construct()
    {
        $this->setScore(0);
        $this->setStatus(self::STATUS_INPROGRESS);
        $this->failedAttempts = 0;
        $this->wrongTrials = array();
        $this->letters = array();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtAuto()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtAuto()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setPlayer(Player $player)
    {
        $this->player = $player;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    protected function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    protected function setScore($score)
    {
        $this->score = $score;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function getWord()
    {
        return $this->word;
    }

    public function setWord($word)
    {
        $this->word = $word;
    }

    public function getFailedAttempts()
    {
        return $this->failedAttempts;
    }

    public function incrementFailedAttempts()
    {
        $this->failedAttempts++;
    }

    public function markAsWin()
    {
        $this->setStatus(self::STATUS_WIN);

        $totalTrials = count($this->letters) + count($this->wrongTrials);
        $wordLen = strlen($this->word);
        $this->setScore(floor(($wordLen / $totalTrials) * 1000));
    }

    public function markAsLoose()
    {
        $this->setStatus(self::STATUS_LOOSE);
    }

    public function getWrongTrials()
    {
        return $this->wrongTrials;
    }

    public function appendWrongTrials($trial)
    {
        array_push($this->wrongTrials, $trial);
    }

    public function appendLetters($letter)
    {
        $keys = array_keys(str_split($this->word), $letter);
        $positions = array();
        foreach ($keys as $key) {
            $positions[$letter][] = $key;
        }
        array_push($this->letters, $positions);
    }

    public function getLetters()
    {
        return $this->letters;
    }

    public function isLetterChosen($chosenLetter)
    {
        foreach ($this->letters as $letters) {
            foreach ($letters as $char => $positions) {
                if ($char === $chosenLetter) {
                    return true;
                }
            }
        }
        return (in_array($chosenLetter, $this->wrongTrials));
    }

    public function isLetterInWord($letter)
    {
        $result = mb_stripos($this->word, $letter);
        return (false !== $result);
    }

    protected function getCurrentChosenLettersAsString()
    {
        $word = array_fill(0, strlen($this->word) - 1, '');
        foreach ($this->letters as $letters) {
            foreach ($letters as $char => $keys) {
                foreach ($keys as $key) {
                    $word[$key] = $char;
                }
            }
        }

        ksort($word);
        return implode("", $word);
    }

    public function isGameWin()
    {
        $word = $this->getCurrentChosenLettersAsString();
        if ($word === $this->word) {
            return true;
        }

        return false;
    }

    public function sanitizeWord()
    {
        $new = preg_replace('@\w@', '_', $this->word);
        return $new;
    }
}
