<?php

namespace Games\HangmanBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Games\HangmanBundle\Entity\Game;

class GameRepository extends EntityRepository
{
    public function getTopGamesByPlayerId($playerId, $limit = null)
    {
        $query = $this->_em->createQuery(
            "SELECT g.createdAt, g.score FROM HangmanBundle:Game g 
            WHERE g.player = :playerId and g.status != :status order by g.createdAt DESC"
        );

        if (!empty($limit)) {
            $query->setMaxResults($limit);
        } else {
            $query->setMaxResults(3);
        }
        $query->setParameter('playerId', $playerId);
        $query->setParameter('status', Game::STATUS_INPROGRESS);

        return $query->getResult();
    }

    public function doesPlayerHasActiveGame($playerId)
    {
        $query = $this->_em->createQuery(
            "SELECT g FROM HangmanBundle:Game g WHERE g.player = :playerId 
            and g.status = :status"
        );

        $query->setParameter('playerId', $playerId);
        $query->setParameter('status', Game::STATUS_INPROGRESS);

        return $query->getResult();
    }

    public function findActiveById($gameId)
    {
        $query = $this->_em->createQuery(
            "SELECT g FROM HangmanBundle:Game g WHERE g.id = :gameId and g.status = :status"
        );

        $query->setParameter('gameId', $gameId);
        $query->setParameter('status', Game::STATUS_INPROGRESS);

        return $query->getOneOrNullResult();
    }
}
