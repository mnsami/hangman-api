'use strict';

hangmanApp.service('playersService', ['$http', 'hangman_api_player_header', 'hangman_api_url', function($http, hangmanApiPlayerHeader, hangmanApiUri){
    var service = {
        // information about currentUser
        currentUser: null,

        topGames: null,

        topPlayers: null,

        // register new user in the backend
        register: function(username, age) {
            var registrationUrl = hangmanApiUri + '/players';
            var request = $http.post(registrationUrl , {'username': username, 'age': age});
            return request.then(function(response) {
                service.currentUser = response.data;
                localStorage.currentUser = angular.toJson(service.currentUser);
                return service.currentUser;
            });
        },

        getTopPlayers: function() {
            if (service.isAuthenticated()) {
                var playerUuid = service.currentUser.uuid;
                var getTopPlayersUrl = hangmanApiUri + '/players';

                var requestHeaders = {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                requestHeaders.headers[hangmanApiPlayerHeader] = playerUuid;

                var request = $http.get(getTopPlayersUrl, requestHeaders);

                return request.then(function (response) {
                    service.topPlayers = response.data;
                    localStorage.topPlayers = angular.toJson(service.topPlayers);
                    return service.topPlayers;
                });
            }
        },

        getTopGames: function() {
            if (service.isAuthenticated()) {
                var playerUuid = service.currentUser.uuid;
                var getTopGamesUrl = hangmanApiUri + '/games';

                var requestHeaders = {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                requestHeaders.headers[hangmanApiPlayerHeader] = playerUuid;

                var request = $http.get(getTopGamesUrl, requestHeaders);

                return request.then(function (response) {
                    service.topGames = response.data;
                    localStorage.topGames = angular.toJson(service.topGames);
                    return service.topGames;
                });
            }
        },
        refreshCurrentUser: function() {
            if (service.isAuthenticated()) {
                var playerUuid = service.currentUser.uuid;
                var getPlayerUrl = hangmanApiUri + '/players/' + playerUuid;

                var requestHeaders = {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                requestHeaders.headers[hangmanApiPlayerHeader] = playerUuid;

                var request = $http.get(getPlayerUrl, requestHeaders);

                return request.then(function (response) {
                    service.currentUser = response.data;
                    localStorage.currentUser = angular.toJson(service.currentUser);
                    return service.currentUser;
                });
            }
        },

        // validates if the user exist in the backend
        requestCurrentUser: function() {
            if (service.isAuthenticated()) {
                return service.currentUser;
            }

            return null;
        },

        // Is the current user authenticated?
        isAuthenticated: function() {
            if (!service.currentUser) {
                // try fetch it from localStorage
                service.currentUser = angular.fromJson(localStorage.currentUser);
            }
            return !!service.currentUser;
        }
    };

    return service;
}]);
